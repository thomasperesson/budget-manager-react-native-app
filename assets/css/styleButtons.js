import { StyleSheet } from "react-native";

export const styleButtons = StyleSheet.create({
    button: {
        margin: 10,
        borderRadius: 999
    }
})