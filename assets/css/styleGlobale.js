import { StyleSheet } from "react-native";

export const styleGlobale = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#fff",
        alignItems: "center",
        justifyContent: "center",
    },
    appTitle: {
        fontSize: 50,
        color: "#002EFA",
        textAlign: "center",
    }
});