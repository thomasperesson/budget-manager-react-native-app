import * as React from 'react';
import { View, Text } from 'react-native';
import { homeScreenStyle } from './css/homeScreenStyle';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';

export function HomeScreen() {
    return (
        <View style={homeScreenStyle.style}>
            {/* Mettre les éléments de ma Home ici */}
        </View>
    );
}
