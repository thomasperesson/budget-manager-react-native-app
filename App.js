import React, { useState, useEffect } from "react";
import { Text, View, Image, StyleSheet } from "react-native";
import { StatusBar } from "expo-status-bar";
import CustomButton from "./components/CustomButton/CustomButton";
import { styleGlobale } from "./assets/css/styleGlobale";
import axios from "axios";

export default function App() {
    const [expenses, setExpenses] = useState([]);
    const [categories, setCategories] = useState([]);
    let expensesData = "https://localhost:8000/api/expenses";
    let categoriesData = "https://localhost:8000/api/categories";
    useEffect(() => {
        axios
            .get(categoriesData)
            .then((response) => {
                setCategories(response.data["hydra:member"]);
            })
            .catch((error) => {
                console.warn(error);
            });
    }, []);

    console.log(categories);

    const styles = StyleSheet.create({
        stretch: {
            width: 50,
            height: 50,
            resizeMode: 'stretch'
        }
    })

    return (
        <View style={styleGlobale.container}>
            <Text style={styleGlobale.appTitle}>BudGet</Text>
            {categories.length > 0 && categories.map((category) => {
                return (
                    <View key={category.id}>
                        <Text>{category.title}</Text>
                        <Image 
                            style={styles.stretch}
                            source={{
                                uri: category.logo
                            }} 
                        />
                    </View>
                )
            })}
            <CustomButton title="Connexion" />
            <CustomButton title="Inscription" />
            <StatusBar style="auto" />
        </View>
    )
}
