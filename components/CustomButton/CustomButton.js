import React from "react";
import { Text, TouchableOpacity } from 'react-native';
import { customButtonStyle } from './css/customButtonStyle';

export default function CustomButton(props) {
    return (
        <TouchableOpacity style={customButtonStyle.customButton}>
            <Text style={customButtonStyle.text}>
                {props.title}
            </Text>
        </TouchableOpacity>
    )
}