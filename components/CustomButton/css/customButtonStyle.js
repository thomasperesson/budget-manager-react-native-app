import { StyleSheet } from 'react-native';

export const customButtonStyle = StyleSheet.create({
    customButton: {
        backgroundColor: "#F1F1F1"
    },
    text: {
        color: "blue"
    }
})